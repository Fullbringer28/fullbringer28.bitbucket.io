(function($) {
    "use strict";
    
    /**
     * START DOCUMENT READY EVENT
     * @param event
     */
    $(function() {
    
    /*--------------------------------------------------------------
      CONTACT FORM AJAXIFY INIT
    --------------------------------------------------------------*/
    $( "#legion-contactform" ).on( "submit", function( e ) {
      
      //Stop form submission & check the validation
      e.preventDefault();
      
      // Variable declaration
      var error       = false,
        name          = $('#legion_name').val(),
        email         = $('#legion_email').val(),
        message         = $('#legion_message').val(),
        mail_fail     = $('#mail_fail'),
        mail_success  = $('#mail_success');
      
      // Form field validation
      if(name.length <= 1){
          var error = true;
          $('#legion_name').parent().addClass('filed_error');
      }else{
          $('#legion_name').parent().removeClass('filed_error');
      }
    
      if(message.length <= 1){
          var error = true;
          $('#legion_message').parent().addClass('filed_error');
      }else{
          $('#legion_message').parent().removeClass('filed_error');
      }
    
      if(email.length <= 6 || email.indexOf('@') == '-1'){
          var error = true;
          $('#legion_email').parent().addClass('filed_error');
      }else{
          $('#legion_email').parent().removeClass('filed_error');
      }
      if (error == true) {
        $(mail_success).fadeOut(500);
        $(mail_fail).slideDown(800);
      };
    
      // If there is no validation error, next to process the mail function
      if(error == false){
          $(mail_success).hide();
          $(mail_fail).hide();
          $.ajax({
          url: $(this).attr('action'),
          data: $(this).serialize(),
          type: 'POST',
          beforeSend: function(){
            $('.form_loader').addClass("mcform_submitting");
          },
          success: function( response ) {
            if ( response ) {
              $(mail_fail).fadeOut(500);
              $(mail_success).slideDown(800);
              $('.legion-form-group input, .legion-form-group textarea').val('');
              $('.filed_error').removeClass('filed_error');
              $('.filed_ok').removeClass('filed_ok');
              $('.form_loader').removeClass("mcform_submitting");
            } else {
              $(mail_success).fadeOut(500);
              $(mail_fail).slideDown(800);
              $('.form_loader').removeClass("mcform_submitting");
            }
          },
          error: function() {
            $(mail_success).fadeOut(500);
            $(mail_fail).slideDown(800);
            $('.form_loader').removeClass("mcform_submitting");
          }
          });
    
      }
    });
    
    /*--------------------------------------------------------------
     14. CONTACT FORM INPUT INAMATION
    --------------------------------------------------------------*/
    $('.legion-form-group input, .legion-form-group textarea').focus(function() {
      $(this).closest('.legion-form-group').addClass('active_item').siblings().removeClass('active_item');
    }).focusout(function() {
      $('.legion-form-group').removeClass('active_item');
    });
    
    
    
    }); // end document ready
    
    
    
    
    
    })(jQuery); // end anonymous function