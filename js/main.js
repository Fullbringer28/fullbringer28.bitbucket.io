(function($) {
    "use strict";
    $(function(){
    
    
    
    
    /*------------------------------------------------------------------
    [Table of contents]
    
    1. legion general functions
    2. legion toggle menu
    3. legion fullscreen menu
    4. legion google map
    5. legion sticky menu
    6. legion appearing scrollup button
    7. legion carousels
    8. legion appeare js
    9. legion popup
    10. legion preloader
    11. legion gallery
    12. legion scroll up
    
    -------------------------------------------------------------------*/
    
    /*--------------------------------------------------------------
     1. legion general functions
    -------------------------------------------------------------*/
    jQuery.fn.is_exists = function(){return this.length>0;}
    
    
    /*--------------------------------------------------------------
     2. legion toggle menu
    -------------------------------------------------------------*/
    if ($('#legion-menu-toggle').is_exists()) {
      $('#legion-menu-toggle').on('click', function () {
          $(this).toggleClass('active');
          $('#fullscreen-menu').toggleClass('open');
      });
    }
    
    /*--------------------------------------------------------------
     3. legion fullscreen menu
    -------------------------------------------------------------*/
    if ($('.legion-fullscreen-menu ul li a').is_exists()) {
      $('.legion-fullscreen-menu ul li a').on('click', function () {
          
          var target = $(this.getAttribute('href'));
    
          $('html, body').animate({
              scrollTop: target.offset().top
          }, 1000);
          
          $('#legion-menu-toggle').removeClass('active');
          $('#fullscreen-menu').removeClass('open');
          
      });
    }
    
    /*--------------------------------------------------------------
     4. legion google map
    -------------------------------------------------------------*/
    if ($('#map').length > 0) {
      google.maps.event.addDomListener(window, 'load', init('map'));
    }
    
    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 16,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            disableDefaultUI: true,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(51.225516, -0.335724), // New York
    
            // How you would like to style the map. 
            // This is where you would paste any style found on Snazzy Maps.
    
                     styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
          };
    
    
        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');
    
        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);
    
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(51.225516, -0.335724),
          map: map,
          icon: 'images/',
          // title: 'Larsia'
        });
        var contentString = '<div id="content">' +
            '<div id="myDiv">' +
            '</div>' +
            '<p>25 East middleton' +
            '<p> 25 southpoint Streat Luxemburg</p>' +
            '<p>US, 1154</p>'+
            '</div>' +
            '</div>';
    
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 280
        });
    
        marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function(){ marker.setAnimation(null); }, 750);  //time it takes for one bounce   
    
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    
    }
    
    
    $(window).on('scroll',function(){
    
      /*--------------------------------------------------------------
       5. legion sticky menu
      -------------------------------------------------------------*/
      if ($(window).scrollTop() > 50) {
        $('.header-top-area').addClass('sticky-menu');
      } else {
        $('.header-top-area').removeClass('sticky-menu');
      }
    
      /*--------------------------------------------------------------
       6. legion appearing scrollup button
      -------------------------------------------------------------*/
      if ($(this).scrollTop() > 100) {
          $('.scrollup').fadeIn();
      } else {
          $('.scrollup').fadeOut();
      }
    
    });
    
    
    /*--------------------------------------------------------------
     7. legion carousels
    -------------------------------------------------------------*/
    if($('.legion-banner-slider').is_exists()){
      $('.legion-banner-slider').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText: ["<i class=\"fa fa-angle-left\"></i>",
            "<i class=\"fa fa-angle-right\"</i>"],
        navigation: true,
        items:1,
        autoplay:true,
        autoplayTimeout:3000,
      });
    }
    
    
    if($('.legion-testimonial-slider').is_exists()){
      $('.legion-testimonial-slider').owlCarousel({
        center: true,
        loop:true,
        margin:10,
        nav:true,
        navText: ["<i class=\"fa fa-angle-left\"></i>",
            "<i class=\"fa fa-angle-right\"</i>"],
        navigation: true,
        items:3,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:3
            },
            1020:{
                items:3
            }
        }
      });
    }
    
    if($('.legion-client-slider').is_exists()){
      $('.legion-client-slider').owlCarousel({
        autoplay:true,
        loop:true,
        margin:10,
        nav:false,
        navText: ["<i class=\"fa fa-angle-left\"></i>",
            "<i class=\"fa fa-angle-right\"</i>"],
        navigation: true,
        items:8,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:4
            },
            1020:{
                items:8
            }
        }
      });
    }
    
    /*--------------------------------------------------------------
     8. legion appeare js
    -------------------------------------------------------------*/
    (function(){
      var number_percentage = $(".legion-portfolio-number-percentage");
    
        number_percentage.appear();
        $(document.body).on('appear', '.legion-portfolio-number-percentage-count', function () {
          number_percentage.each(function () {
            $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-animation-duration"), 10));
            var value = $(this).attr("data-value");
            var duration = $(this).attr("data-animation-duration");
            $(this).closest('.legion-portfolio-single-team-skill').find('.legion-portfolio-team-skill .legion-portfolio-skill-right').animate({width : value+'%'}, 4500);
          });
      });
    
    var appear = $('.appear');
    appear.appear();
    $.fn.animateNumbers = function (stop, commas, duration, ease) {
      return this.each(function () {
        var $this = $(this);
        var start = parseInt($this.text().replace(/,/g, ""), 10);
        commas = (commas === undefined) ? true : commas;
        $({
          value: start
        }).animate({
            value: stop
          }, {
            duration: duration == undefined ? 500 : duration,
            easing: ease == undefined ? "swing" : ease,
            step: function () {
              $this.text(Math.floor(this.value));
              if (commas) {
                $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
              }
            },
            complete: function () {
              if (parseInt($this.text(), 10) !== stop) {
                $this.text(stop);
                if (commas) {
                  $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                }
              }
            }
          });
      });
    }
    })();
    
    /*--------------------------------------------------------------
     9. legion popup
    -------------------------------------------------------------*/
    if($('.legion-single-gallery').is_exists()){
      $('.legion-single-gallery').magnificPopup({
          delegate: 'a',
          type: 'image',
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: 'mfp-with-zoom mfp-img-mobile',
          image: {
              verticalFit: true
          },
          gallery: {
              enabled: true
          },
          zoom: {
              enabled: true,
              duration: 500, // don't foget to change the duration also in CSS
              opener: function(element) {
                  return element.find('img');
              }
          }
          
      });
    }
    
     
    
     });
    
    
    
    $(window).on("load" ,function(){
    
    /*--------------------------------------------------------------
     10. legion preloader
    -------------------------------------------------------------*/
    if($("#preloader").is_exists()){
      $("#preloader").fadeOut(500);
    }
    
    /*--------------------------------------------------------------
     11. legion gallery
    -------------------------------------------------------------*/
    if($('#legion-items').is_exists()){
      var $container = $('#legion-items'),
        colWidth = function () {
          var w = $container.width(), 
            columnNum = 1,
            columnWidth = 0;
          if (w > 1200) {
            columnNum  = 3;
          } else if (w > 900) {
            columnNum  = 3;
          } else if (w > 600) {
            columnNum  = 3;
          } else if (w > 449) {
            columnNum  = 2;
          } else if (w > 320) {
            columnNum  = 1;
          }
          columnWidth = Math.floor(w/columnNum);
          $container.find('.portfolio-item').each(function() {
            var $item = $(this),
              multiplier_w = $item.attr('class').match(/portfolio-item-w(\d)/),
              multiplier_h = $item.attr('class').match(/portfolio-item-h(\d)/),
              width = multiplier_w ? columnWidth*multiplier_w[1] : columnWidth,
              height = multiplier_h ? columnWidth*multiplier_h[1] : columnWidth;
            $item.css({
              width: width,
              height: height
            });
          });
          return columnWidth;
        },
        isotope = function () {
          $container.isotope({
            resizable: false,
            itemSelector: '.portfolio-item',
            masonry: {
              columnWidth: colWidth(),
              gutterWidth: 0
            }
          });
        };
      isotope();
      $(window).on('resize',isotope);
      var $optionSets = $('#watch-filter-gallery'),
          $optionLinks = $optionSets.find('li');
      $optionLinks.on('click',function(){
      var $this = $(this);
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
    
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[ key ] = value;
        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
          // changes in layout modes need extra logic
          changeLayoutMode( $this, options )
        } else {
          // creativewise, apply new options
          $container.isotope( options );
        }
        return false;
      });
    }
    
    
    /*--------------------------------------------------------------
     12. legion scroll up
    -------------------------------------------------------------*/
    if($('.scrollup').is_exists()){
      $('.scrollup').on('click',function () {
          $("html, body").animate({
              scrollTop: 0
          }, 600);
          return false;
      });
    }
    
    
    });
    
    })(jQuery);
    